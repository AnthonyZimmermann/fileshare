import { CommonModule } from '@angular/common';
import { Component, EventEmitter, inject, Input, Output } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { FileIndexEntry } from '../file-index-entry';
import { FileshareDataServerService } from '../fileshare-data-server.service';

@Component({
  selector: 'app-fileshare-view-file-index-entry',
  standalone: true,
  imports: [
    CommonModule,
    FormsModule,
  ],
  templateUrl: './fileshare-view-file-index-entry.component.html',
  styleUrl: './fileshare-view-file-index-entry.component.scss'
})
export class FileshareViewFileIndexEntryComponent {

  private fileshare_data_server_service: FileshareDataServerService = inject(FileshareDataServerService);

  @Input() file_index_entry!: FileIndexEntry;
  @Output() change_into_directory: EventEmitter<string> = new EventEmitter();

  public download_select: boolean = false;

  toggleDownloadSelect(new_value: boolean): void {
    this.download_select = new_value;
    console.log("toggleDownloadSelect...", this.download_select);
  }

  changeIntoDirectory(): void {
    this.change_into_directory.emit(this.file_index_entry.name);
  }

  delete(): void {
    this.fileshare_data_server_service.deleteFile(this.file_index_entry.index_path).subscribe((response) => {
    });
  }

  download(): void {
    const download_url = this.fileshare_data_server_service.getDownloadUrl(this.file_index_entry.index_path);
    window.open(download_url, "_black");
  }
}
