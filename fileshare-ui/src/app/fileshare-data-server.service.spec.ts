import { TestBed } from '@angular/core/testing';

import { FileshareDataServerService } from './fileshare-data-server.service';

describe('FileshareDataServerService', () => {
  let service: FileshareDataServerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FileshareDataServerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
