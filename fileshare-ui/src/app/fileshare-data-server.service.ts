import { HttpClient, HttpRequest, HttpEvent } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { Observable, Subject } from 'rxjs';

import { FileIndexEntry } from './file-index-entry';

@Injectable({
  providedIn: 'root'
})
export class FileshareDataServerService {

  private backend_base_url = `${document.location.origin}/data`

  http_client: HttpClient = inject(HttpClient);

  public index_change = new Subject<void>();

  getFileIndexEntries(path: string): Observable<FileIndexEntry[]> {
    return this.http_client.get<FileIndexEntry[]>(`${this.backend_base_url}/fetch/${path}`);
  }

  getDownloadUrl(path: string): string {
    return `${this.backend_base_url}/download/${path}`;
  }

  uploadFile(path: string, file: File): Observable<HttpEvent<any>> {
    const form_data = new FormData();
    form_data.set('enctype', 'multipart/form-data');
    form_data.append('file', file);

    const request_object = new HttpRequest('POST', `${this.backend_base_url}/upload/${path}`, form_data, {
      reportProgress: true,
      responseType: 'json',
    });

    const request_observable = this.http_client.request(request_object);
    request_observable.subscribe((response) => {
      this.index_change.next();
    });

    return request_observable;
  }

  deleteFile(path: string): Observable<HttpEvent<any>> {
    const request_object = new HttpRequest('POST', `${this.backend_base_url}/delete/${path}`, null);

    const request_observable = this.http_client.request(request_object);
    request_observable.subscribe((response) => {
      this.index_change.next();
    });

    return request_observable;
  }
}
