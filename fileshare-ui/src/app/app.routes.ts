import { Routes } from '@angular/router';
import { FileshareViewComponent } from './fileshare-view/fileshare-view.component';

export const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'fileshare-view/' },
  { path: 'fileshare-view', pathMatch: 'full', redirectTo: 'fileshare-view/' },
  { path: 'fileshare-view/:path', component: FileshareViewComponent, title: 'Fileshare View' },
];
