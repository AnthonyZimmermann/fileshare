import datetime
import json
import os
import re

from filesharedataserver.config import Config
from filesharedataserver.fileutils import FileDataUtils
from filesharedataserver.fileutils import FileData


class IndexEntry:
    INDEX_PATH_PATTERN = re.compile(r"^[a-zA-Z0-9_\-/\.\+]*$")

    def __init__(self, index_path):
        assert bool(FileIndexEntry.INDEX_PATH_PATTERN.match(index_path))
        self._index_path = index_path

    @property
    def index_path(self):
        return self._index_path

    @property
    def name(self):
        return os.path.basename(self.index_path)

    def export_to_dict(self, path_sep=None):
        index_path = self.index_path
        if path_sep is not None:
            index_path = index_path.replace(os.path.sep, path_sep)
        return {
            "index_path": index_path,
            "name": self.name,
        }


class FileIndexEntry(IndexEntry):
    OWNER_PATTERN = re.compile(r"^[a-zA-Z0-9_\-]+$")

    def __init__(self, index_path, owner, modify_dt):
        super().__init__(index_path)
        assert bool(FileIndexEntry.OWNER_PATTERN.match(owner))
        assert isinstance(modify_dt, datetime.datetime)
        self.owner = owner
        self.modify_dt = modify_dt

    @property
    def isodatetime(self):
        return self.modify_dt.isoformat()

    def export_to_dict(self, path_sep=None):
        export_dict = super().export_to_dict(path_sep=path_sep)
        export_dict.update({
            "owner": self.owner,
            "modify_dt": self.isodatetime,
        })
        return export_dict

    @staticmethod
    def import_from_dict(data_dict):
        try:
            return FileIndexEntry(
                index_path=data_dict["index_path"],
                owner=data_dict["owner"],
                modify_dt=datetime.datetime.fromisoformat(data_dict["modify_dt"]),
            )
        except KeyError:
            return None


class DirectoryIndexEntry(IndexEntry):
    def __init__(self, index_path, name):
        super().__init__(index_path)
        self._name = name

    @property
    def name(self):
        return self._name


class Index:
    class NotADirectoryError(Exception):
        pass

    def __init__(self, root_path):
        assert os.path.isdir(root_path)
        self._files = dict()
        self._root_path = root_path
        self._files_index_path = os.path.join(self._root_path, "files.index")
        self.load()

    @staticmethod
    def os_path_splitall(path):
        path_rest = path
        path_list = []
        while path_rest != "":
            path_rest, element = os.path.split(path_rest)
            path_list.insert(0, element)
        return path_list

    def update_index(self, index_path, owner):
        file_index_entry = self._files.get(index_path)
        if file_index_entry is not None:
            if owner != file_index_entry.owner:
                return

        try:
            file_index_entry = FileIndexEntry(index_path, owner, datetime.datetime.utcnow())
        except AssertionError:
            return
        self._update_file_index_entry(file_index_entry)
        self.store()

    def delete_index(self, index_path, owner):
        file_index_entry = self._files.get(index_path)
        if file_index_entry is None:
            return
        if owner != file_index_entry.owner:
            return
        self._delete_file_index_entry(file_index_entry)
        self.store()

    def _update_file_index_entry(self, file_index_entry):
        self._files[file_index_entry.index_path] = file_index_entry

    def _delete_file_index_entry(self, file_index_entry):
        if file_index_entry.index_path in self._files:
            del self._files[file_index_entry.index_path]

    def _get_file_index_entries_it(self, index_path=""):
        return (f for f in sorted(self._files.values(), key=lambda f: f.index_path) if os.path.commonpath((f.index_path, index_path)) == index_path)

    def get_file_index_entries(self, index_path=""):
        return list(self._get_file_index_entries_it(index_path=index_path))

    def _get_index_entries_it(self, index_path=""):
        file_index_entries = self._get_file_index_entries_it(index_path)
        directory_names_yielded = set()
        for f in file_index_entries:
            if f.index_path == index_path:
                raise Index.NotADirectoryError(f"'{index_path}' points to a specific file path and thus cannot be a directory that contains other files.")
            f_directory, _ = os.path.split(f.index_path)
            if f_directory == index_path:
                yield f
                continue
            f_index_path_rest = f.index_path[len(index_path):]
            directory_name = self.os_path_splitall(f_index_path_rest)[0]
            if directory_name not in directory_names_yielded:
                directory_names_yielded.add(directory_name)
                yield DirectoryIndexEntry(index_path, directory_name)

    def get_index_entries(self, index_path):
        return list(self._get_index_entries_it(index_path))

    def store(self):
        with open(self._files_index_path, "w") as file_:
            file_.write(json.dumps([f.export_to_dict() for f in self._files.values()]))

    def load(self):
        self._files = dict()
        if os.path.isfile(self._files_index_path):
            with open(self._files_index_path, "r") as file_:
                for data_dict in json.loads(file_.read()):
                    file_index_entry = FileIndexEntry.import_from_dict(data_dict)
                    if file_index_entry is not None:
                        self._update_file_index_entry(file_index_entry)


class IndexApi:
    class NotADirectoryError(Exception):
        pass

    def __init__(self):
        root_path = Config.get("files_root_path")
        assert isinstance(root_path, str)
        if not os.path.exists(root_path):
            os.makedirs(root_path, exist_ok=True)
        assert os.path.isdir(root_path)
        self._root_path = root_path
        self._index = Index(self._root_path)

    def read_index_path(self, index_path=""):
        try:
            index_entries = self._index.get_index_entries(index_path)
        except Index.NotADirectoryError as e:
            raise IndexApi.NotADirectoryError(str(e))
        file_index_entries = list(filter(lambda entry: isinstance(entry, FileIndexEntry), index_entries))
        directory_index_entries = list(filter(lambda entry: isinstance(entry, DirectoryIndexEntry), index_entries))
        return file_index_entries + directory_index_entries

    def get_download_data(self, index_path_list):

        index_path_list_directory_entries = []
        resolved_directories_index_path_list = []
        for index_path in index_path_list:
            index_path_list_directory_entries.append(index_path)
            file_data_list = self._index.get_file_index_entries(index_path)
            resolved_directories_index_path_list += [f.index_path for f in file_data_list]

        for index_path_to_remove in index_path_list_directory_entries:
            index_path_list.remove(index_path_to_remove)
        index_path_list += resolved_directories_index_path_list

        if len(index_path_list) == 1:
            index_path = index_path_list[0]
            system_index_path = os.path.join(self._root_path, index_path)
            return FileData(system_index_path)
        elif len(index_path_list) > 1:
            file_data_list = [FileData(os.path.join(self._root_path, index_path)) for index_path in index_path_list]
            return FileDataUtils.get_zip_file_data(file_data_list)
        else:
            return None

    def store_file(self, index_path, file_data, owner):
        system_index_path = os.path.join(self._root_path, index_path)
        try:
            FileData(system_index_path).store_data(file_data)
        except NotADirectoryError as e:
            raise IndexApi.NotADirectoryError(str(e))
        self._index.update_index(index_path, owner)

    def delete_file(self, index_path, owner):
        system_index_path = os.path.join(self._root_path, index_path)
        FileData(system_index_path).delete_data()
        self._index.delete_index(index_path, owner)
