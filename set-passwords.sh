#!/bin/bash

read -p "User: " input_user
docker exec -it swag htpasswd -c /config/nginx/.htpasswd ${input_user}

while true
do
    read -p "Do you want to enter two more inputs? (y/n): " choice
    if [ "$choice" != "y" ]
    then
        break
    fi

    read -p "User: " input_user
    docker exec -it swag htpasswd /config/nginx/.htpasswd ${input_user}
done
