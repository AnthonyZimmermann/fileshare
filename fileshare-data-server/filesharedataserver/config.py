import os
import yaml

class Config:
    DEFAULT_CONFIG_DATA = {
        "files_root_path": os.path.expanduser("~/.filesharedata"),
    }

    _config_path = os.path.expanduser("~/.config/filesharedataserver/config.yml")
    _data = None

    @staticmethod
    def use_config(path):
        assert os.path.isfile(path)
        Config._config_path = path
        Config._data = None
        Config.load()

    @staticmethod
    def load():
        if Config._data is None:
            Config._data = Config.DEFAULT_CONFIG_DATA.copy()
            try:
                print(f"loading config from: {Config._config_path}")
                with open(Config._config_path, "r") as file_:
                    Config._data.update(yaml.safe_load(file_))
            except:
                pass

    @staticmethod
    def get(key, *keys):
        Config.load()
        def _get(sub_config, key, *keys):
            sub_config_data = sub_config.get(key)
            if sub_config_data is None or len(keys) == 0:
                return sub_config_data
            return _get(sub_config.get(key), *keys)
        if Config._data is not None:
            return _get(Config._data, key, *keys)
