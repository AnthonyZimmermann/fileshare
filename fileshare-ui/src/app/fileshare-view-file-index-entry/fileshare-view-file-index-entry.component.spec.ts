import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FileshareViewRowComponent } from './fileshare-view-row.component';

describe('FileshareViewRowComponent', () => {
  let component: FileshareViewRowComponent;
  let fixture: ComponentFixture<FileshareViewRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FileshareViewRowComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FileshareViewRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
