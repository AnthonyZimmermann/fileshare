import { CommonModule, Location } from '@angular/common';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Component, inject, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, Router, RouterLink } from '@angular/router';

import { ErrorMessageFooterComponent } from '../error-message-footer/error-message-footer.component';
import { FileIndexEntry } from '../file-index-entry';
import { FileshareViewFileIndexEntryComponent } from '../fileshare-view-file-index-entry/fileshare-view-file-index-entry.component';
import { FileshareDataServerService } from '../fileshare-data-server.service';

interface LinkElement {
  text: string;
  path: string;
}

interface UploadState {
  index: number;
  index_path: string;
  filename: string;
  progress: number;
}

@Component({
  selector: 'app-fileshare-view',
  standalone: true,
  imports: [
    CommonModule,
    ErrorMessageFooterComponent,
    FileshareViewFileIndexEntryComponent,
    FormsModule,
    RouterLink,
  ],
  templateUrl: './fileshare-view.component.html',
  styleUrl: './fileshare-view.component.scss'
})
export class FileshareViewComponent implements OnInit {

  private route: ActivatedRoute = inject(ActivatedRoute);
  private router: Router = inject(Router);
  private location: Location = inject(Location);
  private fileshare_data_server_service: FileshareDataServerService = inject(FileshareDataServerService);

  public root_path!: string;
  public directory_link_elements: LinkElement[] = [];
  public directory_input_value: string = "";
  public file_input_value: any;
  public file_index_entries: FileIndexEntry[] = [];

  public current_upload_states: UploadState[] = [];
  public error_message_footer_value: string | null = null;

  ngOnInit(): void {
    this.route.params.subscribe(p => {
      this.root_path = (p['path']) ? p['path'] : "";
      const root_path_splitted: string[] = ["share", ...this.root_path.split(":")];
      this.directory_link_elements = root_path_splitted.filter((directory: string) => directory !== "").map((directory: string, index: number) => {
        return {
          text: directory,
          path: root_path_splitted.slice(1, (index + 1)).join(":"),
        }
      });
      this.updateView();
      this.fileshare_data_server_service.index_change.subscribe(() => {
        this.updateView();
      });
    });
  }

  updateView(): void {
    this.fileshare_data_server_service.getFileIndexEntries(this.root_path).subscribe((file_index_entries: FileIndexEntry[]) => {
      this.file_index_entries = file_index_entries;
    },
    (error: any) => {
      this.goBack();
      this.error_message_footer_value = error?.error?.detail?.message ?? "Unknown error";
      setTimeout(() => {
        this.error_message_footer_value = null;
      }, 5000);
    });
  }

  goBack(): void {
    this.location.back();
  }

  onDirectoryChangeInput(event: Event): void {
    this.changeIntoDirectory(this.directory_input_value);
    this.directory_input_value = "";
  }

  changeIntoDirectory(directory: string): void {
    let root_path_extension = ""
    if (this.root_path === "") {
      root_path_extension += directory;
    } else {
      root_path_extension += ":" + directory;
    }
    this.fileshare_data_server_service.getFileIndexEntries(this.root_path + root_path_extension).subscribe((file_index_entries: FileIndexEntry[]) => {
      this.router.navigate([this.router.url + root_path_extension]);
    },
    (error: any) => {
      this.error_message_footer_value = error?.error?.detail?.message ?? "Unknown error";
      setTimeout(() => {
        this.error_message_footer_value = null;
      }, 8000);
    });
  }

  onFileUploadInput(event: Event): void {
    const event_target = event.target;
    if (event_target !== null && "files" in event_target) {
      const files: FileList = event_target["files"] as FileList;
      Array.from(files).map((file: File) => {
        const upload_index: number = this.current_upload_states.length;
        const current_upload_state_object: UploadState = {
          index: upload_index,
          index_path: this.root_path,
          filename: file.name,
          progress: 0,
        };
        this.current_upload_states.push(current_upload_state_object);
        this.fileshare_data_server_service.uploadFile(this.root_path, file).subscribe((event) => {
          if (event.type === HttpEventType.UploadProgress) {
            current_upload_state_object.progress = Math.round(95 * (event.loaded / (event.total ?? event.loaded)));
          } else if (event instanceof HttpResponse) {
            current_upload_state_object.progress = 100;
            this.current_upload_states = this.current_upload_states.filter((element: UploadState) => element !== current_upload_state_object);
          }
        });
      });
    }
  }

  download_all(): void {
    const download_url = this.fileshare_data_server_service.getDownloadUrl(this.root_path);
    window.open(download_url, "_black");
  }
}
