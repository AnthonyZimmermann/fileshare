import argparse
import fastapi
import io
import os
import uvicorn

from filesharedataserver.index import IndexApi
from filesharedataserver.config import Config

class FileshareDataServer:
    def __init__(self):
        self._fastapi_app = fastapi.FastAPI()
        self._router = fastapi.APIRouter()

        self._index_api = IndexApi()
        self._setup_routes()

    def _setup_routes(self):
        self._router.add_api_route("/delete/", self.delete_file, methods=["POST"])
        self._router.add_api_route("/delete/{index_path}", self.delete_file, methods=["POST"])
        self._router.add_api_route("/download", self.download_file, methods=["GET"])
        self._router.add_api_route("/download/", self.download_file, methods=["GET"])
        self._router.add_api_route("/download/{index_path}", self.download_file, methods=["GET"])
        self._router.add_api_route("/fetch", self.fetch_list, methods=["GET"])
        self._router.add_api_route("/fetch/", self.fetch_list, methods=["GET"])
        self._router.add_api_route("/fetch/{index_path}", self.fetch_list, methods=["GET"])
        self._router.add_api_route("/upload", self.upload_file, methods=["POST"])
        self._router.add_api_route("/upload/", self.upload_file, methods=["POST"])
        self._router.add_api_route("/upload/{index_path}", self.upload_file, methods=["POST"])
        self._fastapi_app.include_router(self._router)

    def get_fastapi_app(self):
        return self._fastapi_app

    def delete_file(self, index_path):
        index_path = index_path.replace(":", os.path.sep)
        self._index_api.delete_file(index_path, "owner")
        return {"success": index_path}

    def download_file(self, index_path=None):
        if index_path is None:
            index_path = ""
        index_path = index_path.replace(":", os.path.sep)
        file_data = self._index_api.get_download_data([index_path])
        if file_data is None:
            raise fastapi.HTTPException(status_code=422, detail={"error": "no-match", "message": f"No file matches {index_path}"})
        file_data_data = file_data.read_data()
        if file_data_data is None:
            raise fastapi.HTTPException(status_code=422, detail={"error": "no-match", "message": f"No file matches {index_path}"})

        file_data_stream = io.BytesIO(file_data_data)
        return fastapi.responses.StreamingResponse(
            file_data_stream,
            media_type="application/octet-stream",
            headers={'Content-Disposition': f'attachment; filename="{file_data.filename}"'},
        )

    def fetch_list(self, index_path=None):
        if index_path is None:
            index_path = ""
        index_path = index_path.replace(":", os.path.sep)
        try:
            index_entries = self._index_api.read_index_path(index_path)
        except IndexApi.NotADirectoryError as e:
            raise fastapi.HTTPException(status_code=422, detail={"error": "not-a-directory", "message": str(e)})
        return [entry.export_to_dict(path_sep=":") for entry in index_entries]

    def upload_file(self, index_path=None, file: fastapi.UploadFile = fastapi.File(...)):
        if index_path is None:
            index_path = ""
        index_path = index_path.replace(":", os.path.sep)
        file_data = file.file.read()
        try:
            self._index_api.store_file(os.path.join(index_path, file.filename), file_data, "owner")
            return {"success": file.filename}
        except Exception as e:
            raise fastapi.HTTPException(status_code=409, detail={"error": "unknown", "message": str(e)})


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--config", type=str, help="path to config.yml")

    args = parser.parse_args()

    if args.config is not None:
        Config.use_config(args.config)

    fileshare_data_server = FileshareDataServer()
    uvicorn.run(fileshare_data_server.get_fastapi_app(), host="0.0.0.0", port=8000)
