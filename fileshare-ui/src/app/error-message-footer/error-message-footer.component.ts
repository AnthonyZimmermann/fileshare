import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-error-message-footer',
  standalone: true,
  imports: [
    CommonModule,
  ],
  templateUrl: './error-message-footer.component.html',
  styleUrl: './error-message-footer.component.scss'
})
export class ErrorMessageFooterComponent {
  @Input() error_message: string | null = null;

  dismiss(): void {
    this.error_message = null;
  }
}
