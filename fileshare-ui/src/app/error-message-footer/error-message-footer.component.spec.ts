import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorMessageFooterComponent } from './error-message-footer.component';

describe('ErrorMessageFooterComponent', () => {
  let component: ErrorMessageFooterComponent;
  let fixture: ComponentFixture<ErrorMessageFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ErrorMessageFooterComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(ErrorMessageFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
