import datetime
import os
import tempfile
import zipfile


class FileDataBase:
    @property
    def path(self):
        raise NotImplementedError()

    @property
    def filename(self):
        raise NotImplementedError()

    def read_data(self):
        raise NotImplementedError()

    def store_data(self, data):
        raise NotImplementedError()

    def delete_data(self):
        raise NotImplementedError()


class FileData(FileDataBase):
    def __init__(self, path):
        self._path = path

    @property
    def path(self):
        return self._path

    @property
    def filename(self):
        return os.path.basename(self.path)

    def read_data(self):
        if not os.path.isfile(self.path):
            return None
        with open(self.path, "rb") as file_:
            return file_.read()

    def store_data(self, data):
        directory_path = os.path.split(self.path)[0]
        if not os.path.exists(directory_path):
            os.makedirs(directory_path, exist_ok=True)
        with open(self.path, "wb") as file_:
            file_.write(data)

    def delete_data(self):
        if os.path.exists(self.path):
            os.remove(self.path)

            directory_path = os.path.split(self.path)[0]
            if len(os.listdir(directory_path)) == 0:
                os.rmdir(directory_path)


class ZipFileData(FileDataBase):
    def __init__(self, filename, data):
        self._filename = filename
        self._data = data

    @property
    def path(self):
        return None

    @property
    def filename(self):
        return self._filename

    def read_data(self):
        return self._data


class FileDataUtils:
    @staticmethod
    def get_zip_file_data(filedatalist):
        assert all(isinstance(f, FileData) for f in filedatalist)
        all_paths = [f.path for f in filedatalist]
        common_path = os.path.commonpath(all_paths)
        common_path_directory = os.path.split(common_path)[1]
        zip_directory = f"download_{datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')}"
        if common_path_directory != "":
            zip_directory += f"_{common_path_directory}"
        zip_filename = f"{zip_directory}.zip"
        with tempfile.NamedTemporaryFile() as temp_file:
            with zipfile.ZipFile(temp_file, "w") as zip_file:
                for f in filedatalist:
                    zip_path = os.path.join(zip_directory, os.path.relpath(f.path, common_path))
                    zip_file.write(f.path, arcname=zip_path)

            with open(temp_file.name, "rb") as file_:
                return ZipFileData(zip_filename, file_.read())
