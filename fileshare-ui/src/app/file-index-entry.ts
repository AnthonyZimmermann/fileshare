export interface FileIndexEntry {
  index_path: string;
  name: string;
  owner?: string;
  modify_dt?: string;
};
